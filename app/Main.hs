{-# LANGUAGE OverloadedStrings #-}
module Main where

import Data.Monoid
import Web.Spock.Safe
import Data.Text
import Data.Text.IO
import System.Environment
import Control.Monad.IO.Class

main :: IO ()
main =
    getArgs >>= \ arg ->
    runSpock (read (Prelude.head arg) :: Int) $ spockT id $
    do  get root $
            text "we do not have a:\n\n  government\n\n  primeminister"
        
        get ("government") $ 
            noFiller

        get ("primeminister") $
            noFiller

noFiller :: MonadIO m => ActionCtxT ctx m ()
noFiller = do
    htmlText <- liftIO $ getHTMLasText theHTMLPath
    html htmlText

getHTMLasText :: FilePath -> IO Text
getHTMLasText path = 
    Data.Text.IO.readFile path
    
theHTMLPath :: FilePath
theHTMLPath = "./html/no.html"
{-but 
hi this is a comment 

how does this work

start at line 10

get root ~> ~/
    text "Hello World!"
    prints the html text Hello World when you visit /, root

get ### 
    takes a custom URL and returns content based on the URL, in this case using the
    var as a thing ~ /hello/var -> "Hello var", but can be, kiiiinda generic

so let's think about this

we can use this to make, a couple of different subpages kinda trivially, and have 
them grab content from a DB somehow, and display it? 

and then use some sort of styler to like, style it

this should work

--have like, a function for each panel on the site

and then generate divs in the central function that have content from the panel functions

and then and generate content for the panels based on pulling from a DB if necessary, or 
just like a list of text files in markdown


-}